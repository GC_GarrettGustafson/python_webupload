from cx_Freeze import setup, Executable
import os

include_files = [r'C:\Users\Garrett G\AppData\Local\Programs\Python\Python310\DLLs\tcl86t.dll']
# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = ['idna','tkinter','_tkinter'], excludes = [])

base = 'Win32GUI'

executables = [
    Executable('WebUploadGUI.py', base=base)
]

os.environ['TCL_LIBRARY'] = r'C:\Users\Garrett G\AppData\Local\Programs\Python\Python310\tcl\tcl8.6'
os.environ['TK_LIBRARY'] = r'C:\Users\Garrett G\AppData\Local\Programs\Python\Python310\tcl\tk8.6'

setup(name='WebUpload',
      version = '0.91',
      description = 'Performs Upload and format operations on Flex and Global Connect products',
      options = dict(build_exe = buildOptions),
      executables = executables)

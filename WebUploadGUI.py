#! python3
# WebUploadGUI - GUI wrapper for WebUpload.py

#===  IMPORTS ===
import WebUpload, tkinter, os

#===  Global Vars  ===#
version = float(0.95)
ERROR = 0
firmEnable = True #determines whether Upload Firmware is an option in the GUI
firmSelect = False #determines whether there is a selector for the uploaded firmware file
logEnable = True #Saves a log file prior to updating with units firmware rev and current file list.
OEM = ""

try:
    F_CONF = open('config.json','r')
    config = eval(F_CONF.readline())
    #config = {'Format':{'change':True,'active':True},'Webpages':{'change':True,'active':True}, 'Firmware':{'change':True,'active':True},'Firmware_Select':False,'Log_Enable':True}
    F_CONF.close()
    c1Active = config['Format']['change']
    cVar = int(config['Format']['active'])
    c2Active = config['Webpages']['change']
    c2Var = int(config['Webpages']['active'])
    c3Active = config['Firmware']['change']
    c3Var = int(config['Firmware']['active'])
    if "Firmware_Select" in config:
        firmSelect = config['Firmware_Select']
    if "Log_Enable" in config:
        logEnable = config['Log_Enable']
    if "OEM" in config:
        if len(config['OEM']) > 0:
            OEM = "_" + config['OEM']
            # OEM web folders should take format 710-3000_<OEM>
except Exception as e:
    print(e)
    print("Using hard-coded defaults")
    # Checkbox settings
    #    Activate checkboxes (make clickable):
    Changeable = True
    if Changeable:
        c1Active = True  # Format Filesystem Checkbox
        c2Active = True  # Upload Webpages Checkbox
        c3Active = True  # Upload Firmware Checkbox
    else:
        c1Active = False  # Format Filesystem Checkbox
        c2Active = False  # Upload Webpages Checkbox
        c3Active = False  # Upload Firmware Checkbox
    #    Default Checkbox values (checked/unchecked):
    cVar = 0
    c2Var = 1
    c3Var = 1


#=== Functions  ===#

def updateStatus(message):
    global lblStat
    lblStat.configure(text=message)
    global window
    window.update()
        

def cUpdate():
    global cVar
    if (cVar == 1):
        cVar = 0
    else:
        cVar = 1

def c2Update():
    global c2Var
    if (c2Var == 1):
        c2Var = 0
    else:
        c2Var = 1

def c3Update():
    global c3Var
    if (c3Var == 1):
        c3Var = 0
    else:
        c3Var = 1

def Upload():
    ips = ent.get().split(',')#allows for comma separated values
    btn.configure(state="disabled")
    window.update()

    global ERROR
    global firmSelect

    if firmSelect:# If firmware is selectable update firmware file list on each run (allows crude "refresh" without re-running program every build)
        global optionVar1
        global option1
        update_firm_files(get_firm_files(".\\Firmware"), optionVar1, option1) 

    for ip in ips:
        ip = ip.strip()

        content = str(WebUpload.firmware_version_check(ip))
        uType = ''

        # Parse out firmware model
        if '710-' in content:
            tmod = content.split('710-')[1]
            if len(tmod) >= 4:
                uType = '710-' + tmod[:4]
                rev = tmod[5:7]
                print(rev)
        else:
            ERROR = 1
            break

        if uType == '':
            ERROR = 1

        print('uType: ' + uType)
        if content == 1:
            ERROR = 1
            
        global logEnable
        if logEnable == True:
            print("Creating log file!")

            if '710-400' in uType:  # Global Connect
                print("Global Connect")
                url = "http://" + ip + "/api/host/storage/files"
            elif int(rev) >= 22: # flex on -22 or higher
                print("Flex")
                url = "http://" + ip + "/api/host/storage/files/www"
            else: # Flex on under -22
                print("Flex FW <= 21")
                url = "http://" + ip + "/api/v1/files"
            fileList = str(WebUpload.api_get(url))

            log = open('log.' + ip + '.txt', 'w')
            log.write(content + '\n')
            log.write(fileList + '\n')
            log.close()

        if cVar == 1: # Format Filesystem
            print("Format Checked")
            updateStatus("Formatting Filesytem")
            if '710-400' in uType or int(rev) >= 22:  # Global Connect
                url = "http://" + ip + "/api/host/storage/format"
            else: # Flex
                url = "http://" + ip + "/api/v1/disk/format"

            ERROR = WebUpload.format(url)

        if c2Var == 1: # Upload Webpages
            print("Web upload Checked")
            try:
                os.chdir(uType + OEM)
                for filename in os.listdir():
                    if ERROR:
                        updateStatus("Unsuccessful!!!")
                        break
                    if filename.endswith('.gz') or filename.endswith('.html') or filename.endswith('.csv'):
                        if '710-400' in uType:#Global Connect
                            print("Global Connect")
                            url = "http://" + ip + "/api/host/storage/files/"
                            rType = 'PUT'
                        elif int(rev) >= 22: #Flex
                            print("Flex")
                            url = "http://" + ip + "/api/host/storage/files/www/"
                            rType = 'PUT'
                        else:# Flex v1 api
                            print("Flex FW <= 21")
                            url = "http://" + ip + "/api/v1/files/"
                            rType = 'POST'
                        updateStatus("Uploading: " + filename)
                        print(url)
                        print(int(rev))
                        ERROR = WebUpload.upload(url, filename, rType)
                os.chdir('..')
            except:
                ERROR = 1


        if c3Var == 1: # Upload Firmware
            print("Firmware Upload Checked")
            os.chdir('Firmware')
            fileFound = False

            if '710-400' in uType or int(rev) >= 22:  # Global Connect
                            url = "http://" + ip + "/api/host/upgrade"
            else:
                url = "http://" + ip + "/api/v1/version"
                            
            if firmSelect:#Selecting file from optionMenu
                if ERROR:
                        updateStatus("Unsuccessful!!!")
                        pass
                else:
                    filename = optionVar1.get()
                    if filename.endswith('.bin') and uType in filename and uType != '':
                        updateStatus("Uploading: " + filename)
                        print("Uploading...")
                        ERROR = WebUpload.firmware_upload(url,filename)
                        fileFound = True
            else:#uses first compatible .bin file found (alphabetical)
                for filename in os.listdir():
                    if ERROR:
                        updateStatus("Unsuccessful!!!")
                        break
                    if filename.endswith('.bin') and uType in filename and uType != '':
                                     
                        updateStatus("Uploading: " + filename)
                        print("Uploading...")
                        ERROR = WebUpload.firmware_upload(url,filename)
                        fileFound = True
                        break #Only upload 1 firmware file
            os.chdir('..')
            if fileFound == False:
                #updateStatus("Unknown Unit, No firmware found")
                ERROR += 1

        if ERROR == 0 and (c2Var or cVar or c3Var):
            updateStatus("Action Complete")
        elif ERROR != 0:
            updateStatus("Unsuccessful!!!")
            break

    btn.configure(state="active")
    ERROR = 0
     
def get_firm_files(rel_path):
    fileList = os.listdir(rel_path)
    print("fileList: \n" + str(fileList) + "\n\n")
    
    
    for file in fileList:
        print("checking: " + file)
        if ".bin" not in str(file):
            print("removed: " + file)
            fileList.remove(file)
    if 'MoveOldFlexFirms.bat' in fileList:#for some unknown reason the loop above skips this file!?
        fileList.remove('MoveOldFlexFirms.bat')

    return fileList

def update_firm_files(file_list, strVar, dropdown):
    print("Updating File List")
    # Reset var and delete all old options
    dropdown['menu'].delete(0, 'end')
    
    # Insert list of new options (tk._setit hooks them up to var)
    for choice in file_list:
        dropdown['menu'].add_command(label=choice, command=tkinter._setit(strVar, choice))
 
#=== Main ===#

window = tkinter.Tk()
#window.wm_attributes("-topmost",1)

windowX = 240
windowY = 185
if firmEnable == True:
    #window.geometry("240x185")
    pass
else:
    #window.geometry("200x155")
    windowX -= 40
    windowY -= 30
if firmSelect == True:
    windowY += 30

window.geometry(str(windowX)+'x'+str(windowY))

WebUpload.setup()

try:
    window.wm_iconbitmap('favicon.ico')
except:
    print("No icon file available")

window.title('Web Upload v' + str(version))


lbl = tkinter.Label(text='IP address:')
lbl.pack()

ent = tkinter.Entry(window)
ent.pack()

ent.insert(0,'192.168.1.70')

check = tkinter.Checkbutton(window,text="Format Filesystem",command=cUpdate)
if cVar == 1:
    check.select()#Activate for default
check.pack()
if not c1Active:
    check.configure(state="disabled")

check2 = tkinter.Checkbutton(window,text="Upload Webpages",command=c2Update)
if c2Var == 1:
    check2.select()#Activate for default
check2.pack()
if not c2Active:
    check2.configure(state="disabled")

if firmEnable == True:
    check3 = tkinter.Checkbutton(window,text="Upload Firmware",command=c3Update)
    if c3Var == 1:
        check3.select()#Activate for default
    check3.pack()
    if not c3Active:
        check3.configure(state="disabled")

if firmSelect == True:
    optionVar1 = tkinter.StringVar(window)
    optionVar1.set("Select File")
    
    fileList = get_firm_files(".\\Firmware")
        
    option1 = tkinter.OptionMenu(window,optionVar1,*fileList)
    option1.pack()
    
btn = tkinter.Button(text='Upload',command=Upload)
btn.pack()

lbl2 = tkinter.Label(text='Status:')
lbl2.pack()

lblStat = tkinter.Label(text='')
lblStat.pack()

updateStatus("Waiting for input")

window.mainloop()


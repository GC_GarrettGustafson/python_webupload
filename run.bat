@echo off
:: Set Script values here
set script=WebUploadGUI.py
::		Searches for this string in the python path
set python=Python310

:: Automatically determine python path
FOR /F "delims=" %%G IN ('where python.exe') DO (

:: Check if python version matches
echo.%%G | findstr /C:"%python%" 1>nul
if errorlevel 1 (
:: Not a match
echo %%G is not %python%
) ELSE (
:: Match!
echo Python install: %%G
"%%G" %script%
goto exit
)

)

:: Wrong python
echo %python% not found.

:exit


pause
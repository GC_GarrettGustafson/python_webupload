@echo off
:: Set Script name here
set script=WebUploadGUI.py

:: Automatically determine python path and execute script
FOR /F "delims=" %%G IN ('where python.exe') DO (
echo Python install: %%G
"%%G" %script%
goto loop
)
:loop
::Ensures script is only run with the first python install found

::Todo: Determine if python is 2 or 3
pause
// Must be run on Windows OS
1. Create a folder named with the first two parts of every model number the app should support (e.g. 710-4004, 710-3000, etc.)
2. Place webpages for each model within its respective folder
3. Place firmware .bin files inside .\Firmware Note: firmware files must contain the firmware part number
4. Run build.bat
5. Copy all model number folders into build directory TODO: automate in batch or python
5. executable file will be in build directory
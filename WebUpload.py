#! Python3
# WebUpload.py - Uploads gzip and html web files to the Flex Filesystem

import requests, os, sys, tkinter
from multiprocessing import Queue
#import WebUploadGUI
def updateStatus(message):
    print(message)
    
def upload(url, filename, rType):
    print("Uploading: " + filename + "\nTo: " + url)

    try:
        if rType == 'PUT':
            if '.gz' in filename:
                r = requests.put(url + filename.split('.gz')[0] + '.gzip', data=open(filename, 'rb'), headers=eval("{'Content-Encoding':'gzip'}"))
            else:
                r = requests.put(url + filename, data=open(filename, 'rb'), headers=eval("{'Content-Encoding':'gzip'}"))
        elif rType == 'POST':#Flex v1 API
            if '.gz' in filename:
                r = requests.post(url + filename.split('.gz')[0], data=open(filename, 'rb'), headers=eval("{'Content-Encoding':'gzip'}"))
            else:
                r = requests.post(url + filename, data=open(filename, 'rb'), headers=eval("{'Content-Encoding':'gzip'}"))
        else: # Shouldn't happen
            return 1
        if not r.ok:
            updateStatus("!!! ERROR - Upload of " + filename + " was not successful !!!")
            return 1
    except:
        updateStatus("!!! ERROR Occurred !!!")
        return 1
    return 0

def firmware_upload(url, filename):
    print("Uploading: " + filename + "\nTo: " + url)

    try:
        r = requests.post(url, data=open(filename, 'rb'))
        #r = requests.post(url, data=open(filename, 'rb'),proxies={"http": "http://127.0.0.1:8888"})
        if not r.ok:
            updateStatus("!!! ERROR - Upload of " + filename + " was not successful !!!")
            return 1
    except:
        updateStatus("!!! ERROR Occurred !!!")
        return 1
    return 0

def api_get(url):
    try:
        r = requests.get(url)
        if not r.ok:
            updateStatus("ERROR GET from " + url)
            return 1
    except:
        updateStatus("!!! ERROR Occurred !!!")
        return 1
    return r.content

def firmware_version_check(ip):
    print("Checking firmware")
    url = "http://" + ip + "/api/host/id"
    return api_get(url)

def format(url):
    print("Formatting: " + url)
    try:
        f = requests.post(url,data="format")
        if not f.ok:
            updateStatus("!!! ERROR - format not successful !!!")
            return 1
    except:
        updateStatus("!!! ERROR - format not successful !!!")
        return 1
    return 0



def setup():
    # Running the frozen Mac version yields a working directory outside the executable directory
    if getattr(sys, 'frozen',False):# Credit for this goes to stack exchange
        # frozen
        dir = os.path.dirname(sys.executable)
    else:
        # unfrozen
        dir = os.path.dirname(os.path.realpath(__file__))

    os.chdir(dir)

